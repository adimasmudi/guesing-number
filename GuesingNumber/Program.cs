﻿using System;

namespace GuesingNumber // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Guesing Number Game");

            var playerScore = 0;
            
            while (true)
            {
                Console.WriteLine("Would you like to play?\n1. Yes\n2. No");
                try
                {
                    Console.WriteLine("Enter your choice : ");
                    var choice = Console.ReadLine();

                    if (choice == "1")
                    {
                        Random rnd = new Random();
                        int num = rnd.Next(1, 20);

                        var stop = false;
                        var scorePerGame = 20;

                        while (!stop)
                        {
                            if(scorePerGame < 1)
                            {
                                stop = true;
                            }
                            Console.WriteLine("Enter your Number : ");
                            var guessedNumber = Console.ReadLine();
                            var intNum = Int64.Parse(guessedNumber);

                            if (num == intNum)
                            {
                                Console.WriteLine("Congratulations!! your guess was right");
                                stop = true;
                            }
                            else if (num < intNum)
                            {
                                Console.WriteLine("Your guess was to high");
                            }
                            else if (num > intNum)
                            {
                                Console.WriteLine("Your guess was to low");
                            }

                            scorePerGame--;
                        }

                        playerScore += scorePerGame;
                    }
                    else if (choice == "2")
                    {
                        Console.WriteLine("Thank you for using this application");
                        Console.WriteLine("Your Score is {0}", playerScore);
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Your choice was wrong");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

    }
}